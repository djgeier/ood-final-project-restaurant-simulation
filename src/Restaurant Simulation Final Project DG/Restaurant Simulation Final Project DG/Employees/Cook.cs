﻿using Restaurant_Simulation_Final_Project_DG.Food;
using Restaurant_Simulation_Final_Project_DG.SandwichBuilders;

namespace Restaurant_Simulation_Final_Project_DG
{
    /// <summary>
    /// The cook. They make the food normally.
    /// </summary>
    public class Cook : IChef
    {
        /// <summary>
        /// Gets or sets the skill level of the chef.
        /// </summary>
        public double Skill { get; set; }

        /// <summary>
        /// Gets or sets the name of the cook.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Initializes a new instance of the Cook class.
        /// </summary>
        /// <param name="skill">The cook's skill level.</param>
        /// <param name="name">The name of the cook.</param>
        public Cook(double skill, string name)
        {
            this.Skill = skill;
            this.Name = name;
        }

        /// <summary>
        /// Has the cook produce the sandwich.
        /// </summary>
        /// <param name="b">The sandwich builder to base the sandwich on.</param>
        /// <returns>The sandwich once it's made.</returns>
        public IFood Produce(ISandwichBuilder b)
        {
            // Create sandwich.
            Sandwich s = new Sandwich(b.GetOutputName(), this.Skill);

            // Run through builder.
            s.Bread = b.BakeBread();
            s.Meat = b.CookMeat();
            s.Vegetable = b.CutVegetables();

            // Return sandwich.
            return s;
        }
    }
}
