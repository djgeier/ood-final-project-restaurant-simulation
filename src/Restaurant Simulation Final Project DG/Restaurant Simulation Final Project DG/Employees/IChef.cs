﻿using Restaurant_Simulation_Final_Project_DG.Food;
using Restaurant_Simulation_Final_Project_DG.SandwichBuilders;

namespace Restaurant_Simulation_Final_Project_DG
{
    /// <summary>
    /// An interface for employees who can cook in the restaurant.
    /// </summary>
    public interface IChef
    {
        /// <summary>
        /// The employee's skill level.
        /// </summary>
        double Skill { get; set; }

        /// <summary>
        /// Has the employee produce a sandwich.
        /// </summary>
        /// <param name="b">The builder to base the sandwich on.</param>
        /// <returns>The sandwich that is produced.</returns>
        IFood Produce(ISandwichBuilder b);
    }
}
