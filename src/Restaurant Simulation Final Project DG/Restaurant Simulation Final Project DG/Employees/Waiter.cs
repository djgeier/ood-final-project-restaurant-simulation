﻿using Restaurant_Simulation_Final_Project_DG.Food;
using Restaurant_Simulation_Final_Project_DG.SandwichBuilders;

namespace Restaurant_Simulation_Final_Project_DG
{
    /// <summary>
    /// The waiter object. This is the person who takes the order and fills in for the cook.
    /// </summary>
    public class Waiter
    {
        /// <summary>
        /// The name of the waiter.
        /// </summary>
        private string name;

        /// <summary>
        /// The skill level of the waiter.
        /// </summary>
        private double skill;

        /// <summary>
        /// Initializes a new instance of the Waiter class.
        /// </summary>
        /// <param name="name">The name of the waiter.</param>
        /// <param name="skill">The skill level of the waiter when cooking.</param>
        public Waiter(string name, double skill)
        {
            this.name = name;
            this.skill = skill;
        }

        /// <summary>
        /// Gets or sets the waiter's skill level while cooking.
        /// </summary>
        public double Skill
        {
            get
            {
                return this.skill;
            }
            set
            {
                this.skill = value;
            }
        }

        /// <summary>
        /// Gets the waiter's name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
        }

        /// <summary>
        /// Has the waiter try to make the food.
        /// </summary>
        /// <param name="b">The sandwich builder to base the sandwich on.</param>
        /// <returns>The sandwich with diminished quality.</returns>
        public IFood BurnFood(ISandwichBuilder b)
        {
            // Make sandwich.
            Sandwich s = new Sandwich(b.GetOutputName(), this.Skill);

            s.Bread = b.BakeBread();
            s.Meat = b.CookMeat();
            s.Vegetable = b.CutVegetables();

            return s;
        }

    }
}
