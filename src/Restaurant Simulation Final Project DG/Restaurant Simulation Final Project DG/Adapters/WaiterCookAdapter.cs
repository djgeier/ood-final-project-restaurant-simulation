﻿using Restaurant_Simulation_Final_Project_DG.Food;
using Restaurant_Simulation_Final_Project_DG.SandwichBuilders;

namespace Restaurant_Simulation_Final_Project_DG.Adapters
{
    /// <summary>
    /// An adapter for the IChef interface.
    /// </summary>
    public class WaiterCookAdapter : IChef
    {
        /// <summary>
        /// The waiter object taking place of the chef.
        /// </summary>
        private Waiter waiterObj;

        /// <summary>
        /// Initializes a new instance of the WaiterCookAdapter class.
        /// </summary>
        /// <param name="w">The waiter to use.</param>
        public WaiterCookAdapter(Waiter w)
        {
            this.waiterObj = w;
        }

        /// <summary>
        /// Gets or sets the skill level of the waiter.
        /// </summary>
        public double Skill
        {
            get
            {
                return waiterObj.Skill;
            }
            set
            {
                waiterObj.Skill = value;
            }
        }

        /// <summary>
        /// Produces the sandwich for consumption.
        /// </summary>
        /// <param name="b">The sandwich builder we want to use to make it.</param>
        /// <returns>The sandwich.</returns>
        public IFood Produce(ISandwichBuilder b)
        {
            return waiterObj.BurnFood(b);
        }
    }
}
