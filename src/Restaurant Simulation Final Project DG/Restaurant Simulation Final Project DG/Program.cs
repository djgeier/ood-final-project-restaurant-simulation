﻿using Restaurant_Simulation_Final_Project_DG;
using Restaurant_Simulation_Final_Project_DG.Adapters;
using Restaurant_Simulation_Final_Project_DG.SandwichBuilders;
using System;

namespace RestaurantConsole
{
    /// <summary>
    /// The main class of the program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main entry point of the application.
        /// </summary>
        /// <param name="args">The command line arguments for the program.</param>
        public static void Main(string[] args)
        {
            // Scenario 1: Cook is present.
            Waiter waiter = new Waiter("Jeff", 0.1);
            IChef c = new Cook(0.95, "Fred");
            Restaurant r = new Restaurant("Fred's Sandwiches", 4.5, waiter, c);

            Console.WriteLine($"Welcome to {r.ToString()}!");
            Console.WriteLine("Please enter your name.");
            string name = Console.ReadLine();
            Customer customer = new Customer();
            customer.Name = name;
            customer.Order = Program.GetSandwichName();

            Console.WriteLine($"The customer goes into the restaurant and orders their {customer.Order.GetOutputName()}");
            r.SeatCustomer(customer);

            // Rating.
            r.Rating += customer.Happiness;
            Console.WriteLine($"As the customer leaves, they give the restaurant a rating. It is now at {Math.Round(r.Rating)}");
            Console.ReadLine();

            // Scenario 2: Cook is absent.
            Console.WriteLine("--Scenario 2--\nWe will assume that your name hasn't changed.");
            c = new WaiterCookAdapter(waiter);
            r = new Restaurant("Dillon's Sandwiches", 1, waiter, c);
            Console.WriteLine($"Welcome to {r.ToString()}!");
            Console.WriteLine("Unfortunately, our regular chef got food poisoning and cannot attend.");
            Console.WriteLine("Your waiter, " + waiter.Name + " will be preparing your order.");
            customer.Order = Program.GetSandwichName();

            Console.WriteLine($"The customer goes into the restaurant and orders their {customer.Order.GetOutputName()}");
            r.SeatCustomer(customer);

            // Rating.
            r.Rating += customer.Happiness;
            Console.WriteLine($"As the customer leaves, they give the restaurant a rating. It is now at {Math.Round(r.Rating)}");
            Console.ReadLine();
        }

        /// <summary>
        /// A simple utility method to make the sandwich builders.
        /// </summary>
        /// <returns>The requested sandwich's sandwich builder.</returns>
        public static ISandwichBuilder GetSandwichName()
        {
            bool get = false;
            ISandwichBuilder output = null;
            while (!get)
            {
                try
                {
                    Console.WriteLine("Please indicate what type of sandwich you want (BLT/Hamburger)");
                    string name = Console.ReadLine();
                    output = SandwichBuilderFactory.Make(name);
                    get = true;
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return output;
        }
    }
}
