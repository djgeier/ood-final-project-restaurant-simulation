﻿namespace Restaurant_Simulation_Final_Project_DG.Food
{
    /// <summary>
    /// A class to hold information about sandwich vegetables.
    /// </summary>
    public class Vegetable : IFood
    {
        /// <summary>
        /// The name of the vegetable.
        /// </summary>
        private string name;

        /// <summary>
        /// The quality of the vegetable.
        /// </summary>
        private double quality;

        /// <summary>
        /// Gets or sets the name of the vegetable.
        /// </summary>
        public string Name
        {
            get => this.name;
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the quality of the vegetable.
        /// </summary>
        public double Quality
        {
            get => this.quality;
            set
            {
                this.quality = value;
            }
        }
    }
}
