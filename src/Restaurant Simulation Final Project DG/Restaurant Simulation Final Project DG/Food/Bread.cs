﻿namespace Restaurant_Simulation_Final_Project_DG.Food
{
    /// <summary>
    /// A class to represent a bun on a sandwich.
    /// </summary>
    public class Bread : IFood
    {
        /// <summary>
        /// The name of the bread.
        /// </summary>
        private string name;

        /// <summary>
        /// The quality of the bread.
        /// </summary>
        private double quality;

        /// <summary>
        /// Gets or sets the name of the bread.
        /// </summary>
        public string Name
        {
            get => this.name;
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the quality of the bread.
        /// </summary>
        public double Quality
        {
            get => this.quality;
            set
            {
                this.quality = value;
            }
        }
    }
}
