﻿namespace Restaurant_Simulation_Final_Project_DG.Food
{
    /// <summary>
    /// An interface for food.
    /// </summary>
    public interface IFood
    {
        /// <summary>
        /// Gets the name of the food.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the quality of the food.
        /// </summary>
        double Quality { get; }
    }
}
