﻿namespace Restaurant_Simulation_Final_Project_DG.Food
{
    /// <summary>
    /// A class to represent the meat on the sandwich.
    /// </summary>
    public class Meat : IFood
    {
        /// <summary>
        /// The name of the meat.
        /// </summary>
        private string name;

        /// <summary>
        /// The quality of the meat.
        /// </summary>
        private double quality;

        /// <summary>
        /// Gets or sets the name of the meat.
        /// </summary>
        public string Name 
        { 
            get => this.name; 
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the quality of the meat.
        /// </summary>
        public double Quality
        {
            get => this.quality;
            set
            {
                this.quality = value;
            }
        }
    }
}
