﻿using System;

namespace Restaurant_Simulation_Final_Project_DG.Food
{
    /// <summary>
    /// A class to house sandwich ingredients.
    /// </summary>
    public class Sandwich : IFood
    {
        /// <summary>
        /// The name of the sandwich.
        /// </summary>
        private string name;

        /// <summary>
        /// A random variable to add randomness to quality.
        /// </summary>
        private Random random;

        /// <summary>
        /// A value to act as a modifier to the quality of the food.
        /// </summary>
        private double qualityModifier;

        /// <summary>
        /// Initializes a new instance of the Sandwich class.
        /// </summary>
        /// <param name="name">The name of the sandwich.</param>
        /// <param name="skillModifier">The skill modifier of the chef.</param>
        public Sandwich(string name, double skillModifier)
        {
            random = new Random();
            this.name = name;
            qualityModifier = random.NextDouble() + skillModifier;
        }

        /// <summary>
        /// Gets or sets the bread.
        /// </summary>
        public IFood Bread { get; set; }

        /// <summary>
        /// Gets or sets the meat.
        /// </summary>
        public IFood Meat { get; set; }

        /// <summary>
        /// Gets or sets the vegetables.
        /// </summary>
        public IFood Vegetable { get; set; }

        /// <summary>
        /// Gets the name of the sandwich.
        /// </summary>
        public string Name
        {
            get => this.name;
        }

        /// <summary>
        /// Gets the quality of the sandwich.
        /// </summary>
        public double Quality
        {
            get
            {
                return (this.Bread.Quality + this.Meat.Quality + this.Vegetable.Quality) * qualityModifier;
            }
        }

        /// <summary>
        /// Gets a string print out representing the sandwich.
        /// </summary>
        /// <returns>The representation of the sandwich.</returns>
        public override string ToString()
        {
            return $"A {this.Name} sandwich with {this.Meat.Name} and {this.Vegetable.Name} topped with a {this.Bread.Name} bun.";
        }
    }
}
