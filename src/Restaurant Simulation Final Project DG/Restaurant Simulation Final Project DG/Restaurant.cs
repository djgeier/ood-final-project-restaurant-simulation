﻿using System;
using System.Collections.Generic;
using Restaurant_Simulation_Final_Project_DG.Food;
using Restaurant_Simulation_Final_Project_DG.SandwichBuilders;

namespace Restaurant_Simulation_Final_Project_DG
{
    /// <summary>
    /// A class to represent the restaurant.
    /// </summary>
    public class Restaurant
    {
        /// <summary>
        /// The name of the restaurant.
        /// </summary>
        private string name;

        /// <summary>
        /// The average rating of the restaurant.
        /// </summary>
        private double rating;

        /// <summary>
        /// The restaurant's waiter.
        /// </summary>
        private Waiter waiter;

        /// <summary>
        /// The restaurant's chef.
        /// </summary>
        private IChef chef;

        /// <summary>
        /// The list of customers in the restaurant.
        /// </summary>
        private List<Customer> customers = new List<Customer>();

        /// <summary>
        /// Initializes a new instance of the Restaurant class.
        /// </summary>
        /// <param name="name">The name of the restaurant.</param>
        /// <param name="baseRating">The base line rating of the restaurant.</param>
        /// <param name="waiter">The restaurant's waiter.</param>
        /// <param name="chef">The restaurant's chef.</param>
        public Restaurant(string name, double baseRating, Waiter waiter, IChef chef)
        {
            this.name = name;
            this.rating = baseRating;
            this.waiter = waiter;
            this.chef = chef;
        }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        public double Rating
        {
            get
            {
                return this.rating;
            }
            set
            {
                this.rating = value;
            }
        }

        /// <summary>
        /// Seats a customer.
        /// </summary>
        /// <param name="c">The customer to serve.</param>
        public void SeatCustomer(Customer c)
        {
            customers.Add(c);
            c.OnReadyToOrder += this.OnReadyToOrder;
            c.GiveMenu();
        }

        /// <summary>
        /// Called when a customer is ready to order their food.
        /// </summary>
        /// <param name="builder">The builder to use in the sandwich.</param>
        /// <param name="callOnReady">The delegate to call when the food is ready.</param>
        public void OnReadyToOrder(ISandwichBuilder builder, Action<IFood> callOnReady)
        {
            // Prepare food.
            IFood food = this.chef.Produce(builder);

            // Call food ready.
            callOnReady?.Invoke(food);
        }

        /// <summary>
        /// Gets a string representation of the restaurant.
        /// </summary>
        /// <returns>A string containing the name and rating of the restaurant.</returns>
        public override string ToString()
        {
            return $"{name}, the {Math.Round(rating)} star restaurant";
        }
    }
}
