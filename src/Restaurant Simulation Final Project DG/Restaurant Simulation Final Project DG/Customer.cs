﻿using System;
using Restaurant_Simulation_Final_Project_DG.Food;
using Restaurant_Simulation_Final_Project_DG.SandwichBuilders;

namespace Restaurant_Simulation_Final_Project_DG
{
    /// <summary>
    /// A class to represent a customer.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// The name of the customer.
        /// </summary>
        private string name;

        /// <summary>
        /// The customer's happiness.
        /// </summary>
        private double happiness;

        /// <summary>
        /// The action to call when the customer is ready to order.
        /// </summary>
        private Action<ISandwichBuilder, Action<IFood>> onReadyToOrder;

        /// <summary>
        /// The order that the customer has.
        /// </summary>
        public ISandwichBuilder Order { get; set; }

        /// <summary>
        /// Gets or sets the action to call for when the customer is ready to order.
        /// </summary>
        public Action<ISandwichBuilder, Action<IFood>> OnReadyToOrder
        {
            get
            {
                return this.onReadyToOrder;
            }
            set
            {
                this.onReadyToOrder = value;
            }
        }

        /// <summary>
        /// Gets or sets the customer's name.
        /// </summary>
        public string Name
        {
            get => this.name;
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets the happiness level of the customer.
        /// </summary>
        public double Happiness
        {
            get
            {
                return this.happiness;
            }
        }

        /// <summary>
        /// Called when the customer receives their food.
        /// </summary>
        /// <param name="f">The food that is being served.</param>
        public void ReceiveFood(IFood f)
        {
            Console.WriteLine($"The customer eats the {f.Name}.");
            if (f.Quality >= 5)
            {
                Console.WriteLine("The customer is extremely happy with the food that was brought to them.");
                happiness = 1;
            }
            else if (f.Quality >= 3)
            {
                Console.WriteLine("The customer feels that there wasn't anything special to the sandwich.");
                happiness = 0.5;
            }
            else if (f.Quality >= 1)
            {
                Console.WriteLine("The customer did not like their sandwich.");
                happiness = -0.5;
            }
            else
            {
                Console.WriteLine("The customer spit out the sandwich.");
                happiness = -1;
            }
        }

        /// <summary>
        /// Called when the customer is seated.
        /// </summary>
        public void GiveMenu()
        {
            Console.WriteLine($"{this.name} gives the waiter their order.");
            this.OnReadyToOrder?.Invoke(this.Order, this.ReceiveFood);
        }
    }
}