﻿using Restaurant_Simulation_Final_Project_DG.SandwichBuilders;
using System;

namespace Restaurant_Simulation_Final_Project_DG
{
    /// <summary>
    /// A utility class to make sandwich builders based on string representations.
    /// </summary>
    public static class SandwichBuilderFactory
    {
        /// <summary>
        /// Creates a sandwich builder based on the type.
        /// </summary>
        /// <param name="type">The type of sandwich to make.</param>
        /// <returns>The sandwich's appropriate sandwich builder.</returns>
        public static ISandwichBuilder Make(string type)
        {
            if (type.ToLower().Equals("blt"))
            {
                return new BLTBuilder();
            } 
            else if (type.ToLower().Equals("hamburger"))
            {
                return new HamburgerBuilder();
            } else
            {
                throw new ArgumentException("You must specify either BLT or Hamburger.");
            }
        }
    }
}
