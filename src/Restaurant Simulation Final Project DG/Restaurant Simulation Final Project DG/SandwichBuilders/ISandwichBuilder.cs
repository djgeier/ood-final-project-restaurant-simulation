﻿using Restaurant_Simulation_Final_Project_DG.Food;

namespace Restaurant_Simulation_Final_Project_DG.SandwichBuilders
{
    /// <summary>
    /// An interface for building sandwiches.
    /// </summary>
    public interface ISandwichBuilder
    {
        /// <summary>
        /// Bakes the bread for the sandwich.
        /// </summary>
        /// <returns>The bread used on the sandwich.</returns>
        Bread BakeBread();

        /// <summary>
        /// Cuts the vegetables for the sandwich.
        /// </summary>
        /// <returns>The vegetables used on the sandwich.</returns>
        Vegetable CutVegetables();

        /// <summary>
        /// Cooks the meat for the sandwich.
        /// </summary>
        /// <returns>The meat on the sandwich.</returns>
        Meat CookMeat();

        /// <summary>
        /// Gets the output name of the sandwich.
        /// </summary>
        /// <returns>The output name of the sandwich.</returns>
        string GetOutputName();
    }
}
