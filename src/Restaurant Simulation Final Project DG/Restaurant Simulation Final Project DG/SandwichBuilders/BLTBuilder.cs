﻿using Restaurant_Simulation_Final_Project_DG.Food;

namespace Restaurant_Simulation_Final_Project_DG.SandwichBuilders
{
    /// <summary>
    /// Builds a BLT.
    /// </summary>
    public class BLTBuilder : ISandwichBuilder
    {
        /// <summary>
        /// Makes the bread object.
        /// </summary>
        /// <returns>Whole-Grain bread.</returns>
        public Bread BakeBread()
        {
            // Make bread object.
            Bread b = new Bread();
            b.Name = "Whole-Grain Bread";
            b.Quality = 0.9;

            return b;
        }

        /// <summary>
        /// Cooks the meat for the sandwich.
        /// </summary>
        /// <returns>Bacon.</returns>
        public Meat CookMeat()
        {
            // Make bacon
            Meat m = new Meat();
            m.Name = "Bacon";
            m.Quality = 0.85;

            return m;
        }

        /// <summary>
        /// Cuts the vegetables for the sandwich.
        /// </summary>
        /// <returns>The vegetable for the sandwich.</returns>
        public Vegetable CutVegetables()
        {
            // Lettuce and tomatos.
            Vegetable v = new Vegetable();
            v.Name = "Lettuce and Tomatos";
            v.Quality = 0.85;

            return v;
        }

        /// <summary>
        /// Gets the name of the sandwich output.
        /// </summary>
        /// <returns>The name of the sandwich.</returns>
        public string GetOutputName()
        {
            return "BLT";
        }
    }
}
