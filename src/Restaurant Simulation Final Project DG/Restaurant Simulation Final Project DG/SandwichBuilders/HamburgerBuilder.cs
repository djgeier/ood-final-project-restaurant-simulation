﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Restaurant_Simulation_Final_Project_DG.Food;

namespace Restaurant_Simulation_Final_Project_DG.SandwichBuilders
{
    /// <summary>
    /// Build hamburgers.
    /// </summary>
    public class HamburgerBuilder : ISandwichBuilder
    {
        /// <summary>
        /// Makes white bread for hamburgers.
        /// </summary>
        /// <returns>White bread.</returns>
        public Bread BakeBread()
        {
            // Make white bread.
            Bread b = new Bread();
            b.Name = "White Bread";
            b.Quality = 0.85;

            return b;
        }

        /// <summary>
        /// Cooks the meat for the hamburger.
        /// </summary>
        /// <returns>Beef.</returns>
        public Meat CookMeat()
        {
            // Make hamburger
            Meat m = new Meat();
            m.Name = "Beef";
            m.Quality = 0.8;

            return m;
        }

        /// <summary>
        /// Makes the tomatos for the hamburger.
        /// </summary>
        /// <returns>Tomatos.</returns>
        public Vegetable CutVegetables()
        {
            // Make tomato.
            Vegetable v = new Vegetable();
            v.Name = "Tomato";
            v.Quality = 0.9;

            return v;
        }

        /// <summary>
        /// Gets the output name of the sandwich.
        /// </summary>
        /// <returns>The output name of the sandwich.</returns>
        public string GetOutputName()
        {
            return "Hamburger";
        }
    }
}